#!/usr/bin/env bash

apt-get update
apt-get install -y language-pack-en-base python-software-properties

LC_ALL=en_US.UTF-8

add-apt-repository -y ppa:ondrej/php5-5.6

apt-get update
apt-get install -y php5 php5-mysql php5-mcrypt ruby rubygems

gem install sass compass

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/local/bin --filename=composer
php -r "unlink('composer-setup.php');"

debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server-5.5 mysql-server/root_password_again password root'

apt-get install -y mysql-server-5.5

mysqladmin -uroot -proot create website

mysql -uroot -proot -e 'CREATE USER "root"@"%" IDENTIFIED BY "root"'
mysql -uroot -proot -e 'GRANT ALL ON *.* TO "root"@"%"'

sed -i 's/^bind-address.*=.*$/bind-address=0.0.0.0/g' /etc/mysql/my.cnf

service mysql restart